# SymfonyDemo.nix

### Want to test ?

```shell
docker run --rm --tmpfs /tmp -p 8000:8000 registry.gitlab.com/charlycoste/symfony-demo.nix/demo:latest
```

Then launch your browser and go to http://127.0.0.1:8000

You can even test "production" mode with…

```shell
docker run -e APP_ENV=prod -e APP_DEBUG=false --rm --tmpfs /tmp -p 8000:8000 registry.gitlab.com/charlycoste/symfony-demo.nix/demo:latest
```
