{
  pkgs ? import <nixpkgs> { },
  CI_REGISTRY
}:

let

  myphp = pkgs.php81.buildEnv {

    cgiSupport = false;
    fpmSupport = false;
    pharSupport = true; # needed by composer
    phpdbgSupport = false;
    systemdSupport = false;
    ipv6Support = false;
    valgrindSupport = false;
    apxs2Support = false;

    extensions = { all, ... }: with all; [
      pdo_sqlite
      filter
      mbstring
      openssl
      tokenizer
      ctype
      dom
      session
      simplexml
      iconv
    ];
  };

  demo = pkgs.stdenv.mkDerivation {
    name = "demo-symfony";

    buildPhase = ''
      SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
      ${myphp.packages.composer}/bin/composer install --prefer-dist --optimize-autoloader
      ${myphp}/bin/php bin/console cache:clear --no-warmup
    '';

    installPhase = ''
      cp -r . $out/
    '';

    src = builtins.fetchGit {
      url = "https://github.com/symfony/demo";
      ref = "refs/tags/v2.1.2";
      rev = "bf70bfe22fafdc7904128d82624861da8d2b626e";
    };
  };

in
{

  tarball = pkgs.stdenv.mkDerivation {
    name = "demo-symfony.tar.gz";
    dontBuild = true;
    src = demo;
    installPhase = "tar cfz $out .";
  };

  docker = pkgs.dockerTools.buildImage
    {
      name = "${CI_REGISTRY}/charlycoste/symfony-demo.nix/demo";
      config.Cmd = [ "${myphp}/bin/php" "-S" "0.0.0.0:8000" "-t" "${demo}/public" ];
    };
}
